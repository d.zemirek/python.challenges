def invest(amount, rate, years):
    """That tracks growing amounts of investmets over time"""
    for year in range(1, years + 1):
        amount = amount * (rate + 1)
        print(f"year {year} : ${amount:.2f}")


amount = float(input("Enter an initial amount: "))
rate = float(input("Enter an annual percentage rate: "))
years = int(input("Enter a number of years: "))
invest(amount, rate, years)