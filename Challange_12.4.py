import pathlib

source = pathlib.Path("/Users/dominikzemirek/Desktop/python-basics-exercises-master/ch12-file-input-and-output/practice_files/documents")
destination = pathlib.Path("/Users/dominikzemirek/Desktop/python-basics-exercises-master/ch12-file-input-and-output/practice_files/images")

for path in source.rglob("*.*"):
    if path.suffix.lower() in [".png", ".jpg", ".gif"]:
        path.replace(destination / path.name)


