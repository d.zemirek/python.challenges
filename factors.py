user_input = int(input("Enter a positive integer: "))

if user_input <= 0:
    print("It's not a positive number")
else:
    for factor in range(1, user_input + 1):
        if user_input % factor == 0:
            print(f"{factor} is a factor of {user_input}")
    

   