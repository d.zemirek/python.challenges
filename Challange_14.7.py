from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path

class PdfFileUnscramble:
    def __init__(self, pdf_path):
        self.pdf_path = pdf_path
        self.reader_path = PdfFileReader(self.pdf_path)
        self.writer = PdfFileWriter()

    def decryption_pdf(self):
        self.reader_path.decrypt(password = "SuperSecret")
# Rotate pages to neutral postion
    def rotate_pages(self):
        for page in self.reader_path.pages:
            rotation_degress = page["/Rotate"]
            if rotation_degress != 0:
                page.rotateCounterClockwise(rotation_degress)
                
    def get_page(self, page):
        return page.extractText()
# Sort pages from 1 to 7
    def sort_pages(self):
        pages = list(self.reader_path.pages)
        pages.sort(key=self.get_page)
        for slide in pages:
          self.writer.addPage(slide)

    def write_pdf(self):
        with open("unscramabled.pdf", mode='wb') as output_file:
            self.writer.write(output_file) 

pdf_path = "/Users/dominikzemirek/Desktop/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/scrambled.pdf"
unscramble_pdf = PdfFileUnscramble(pdf_path)
unscramble_pdf.sort_pages()
unscramble_pdf.rotate_pages()
unscramble_pdf.write_pdf()

    
