from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path


class PdfFileSplitter:
    def __init__(self, pdf_path):
        self.reader_pdf = PdfFileReader(pdf_path)
        self.writer_1 = None
        self.writer_2 = None

# Add pages but not including breakpoint
    def split(self, breakpoint):
        self.writer_1 = PdfFileWriter()
        self.writer_2 = PdfFileWriter()
        
        for page in self.reader_pdf.pages[:breakpoint]:
            self.writer_1.addPage(page)

        for page in self.reader_pdf.pages[breakpoint:]:
            self.writer_2.addPage(page)
        
# Open 2 files in write binary
    def write(self, filename):
        with Path(filename + "_1.pdf").open(mode="wb") as output_file:
            self.writer_1.write(output_file)

        with Path(filename + "_2.pdf").open(mode="wb") as output_file:
            self.writer_2.write(output_file)
        


pdf_splitter = PdfFileSplitter ("/Users/dominikzemirek/Desktop/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/Pride_and_Prejudice.pdf")
pdf_splitter.split(breakpoint=150)
pdf_splitter.write("mydoc_split")




