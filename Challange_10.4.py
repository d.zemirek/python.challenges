import random
class Animal:
    species = 1

    def __init__(self, name, color):
        self.name = name
        self.color = color
        

    def __str__(self):
        return f"{self.name} is {self.color}"

    def speak(self, sound):
        return f"{self.name} sound like this: {sound}"

    def walking(self, number_of_leg):
        return f"{self.name} walks on {number_of_leg} legs "

    def sleep(self, hours):
        hours = random.randint(1, 8)
        if hours < 5:
            print(f"{self.name} need more sleep")
        else:
            print(f"{self.name} do not need more sleep")
        return hours
        
        
    def run(self, km):
        return f"{self.name} run {km} km/h"

    def food(self, eat):
         
        return f"{self.name} eat {eat}kg food per day"


class Cow(Animal):
    species = "Cow"

    def sleep(self, hours=1):
        return f"She slept {super().sleep(hours)} hours"

class Chicken(Animal):
    species = "Chicken"

    def __init__(self, name, color, egg):
        super().__init__(name, color)
        self.egg = egg

    def __str__(self):
        return f"{super().__str__()} and he lays {self.egg} eggs per day"
    
    def walking(self, number_of_leg=2):
        return super().walking(number_of_leg)

    def sleep(self, hours=1):
        return super().sleep(hours)

    def run(self, km=9):
        return super().run(km)

    def food(self, eat=0.16):
        return super().food(eat)




class Duck(Animal):
    species = "Duck"

    def __init__(self, name, color, weight):
        super().__init__(name, color)
        self.weight = weight

    def __str__(self):
        return f"{Big_Ducky.species} weight {self.weight} kg"


    def speak(self, sound="Kwa Kwa"):
        return super().speak(sound)

    def walking(self, number_of_leg=2):
        return super().walking(number_of_leg)

    def sleep(self, hours=1):
        return super().sleep(hours)

    def run(self, km=11):
        return super().run(km)

    def food(self, eat=0.22):
        return super().food(eat)


Big_Cow = Cow("Cowi", "white")
Big_Ducky = Duck("Ducky", "green", 3)
Big_Chicken = Chicken("Chicki", "brown", 3)


print()
print(Big_Cow)
print(Big_Chicken)
print(Big_Ducky)

print(Big_Cow.speak("MUUU"))
print(Big_Chicken.speak("KO KO KO"))
print(Big_Cow.sleep())
print(Big_Chicken.walking())
print()



  

    

