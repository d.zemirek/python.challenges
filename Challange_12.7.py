import csv

with open('/Users/dominikzemirek/Desktop/python-basics-exercises-master/ch12-file-input-and-output/practice_files/scores.csv', 'r') as csv_file:
    reader = csv.DictReader(csv_file)
    

    high_scores = {}

    for row in reader:
        name = row['name']
        score = int(row['score'])
        if name in high_scores:
            high_scores[name] = max(high_scores[name], score)
        else:
            high_scores[name] = score

with open('/Users/dominikzemirek/Desktop/python-basics-exercises-master/ch12-file-input-and-output/practice_files/high_scores.csv', 'w') as output_file:
    output_writer = csv.writer(output_file)
    output_writer.writerow(['name', 'high_score'])
    for name, score in high_scores.items():
        output_writer.writerow([name, score])