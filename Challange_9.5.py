import random 

Nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
Verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
Adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
Preposition = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
Adverbs = ["curiously", "extravangantly", "tantalizingly", "furiously", "sensuosly"]

def poem():
    n1 = random.choice(Nouns)
    n2 = random.choice(Nouns)
    n3 = random.choice(Nouns)

    while n1 == n2:
        n2 = random.choice(Nouns)
    while n1 == n3 or n2 == n3:
        n3 = random.choice(Nouns)

    v1 = random.choice(Verbs)
    v2 = random.choice(Verbs)
    v3 = random.choice(Verbs)
    
    while v1 == v2:
        v2 = random.choice(Verbs)
    while v1 == v3 or v2 == v3:
        v3 = random.choice(Verbs)

    adj1 = random.choice(Adjectives)
    adj2 = random.choice(Adjectives)
    adj3 = random.choice(Adjectives)

    while adj1 == adj2:
        adj2 = random.choice(Adjectives)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(Adjectives)

    p1 = random.choice(Preposition)
    p2 = random.choice(Preposition)
    
    while p1 == p2:
        p2 = random.choice(Preposition)

    a1 = random.choice(Adverbs)

    if adj1 == "a" or adj1 == "e" or adj1 == "i" or adj1 == "o" or adj1 == "u":
        first_verb = "A"
    else:
        first_verb = "An"

    make_poem =(
        f"{first_verb} {adj1} {n1}\n\n"
        f"{first_verb} {adj1} {n1} {v1} {p1} the {adj2} {n2}\n"
        f"{a1}, the {n1} {v2}\n"
        f"the {n2} {v3} {p2} a {adj3} {n3}"
    )
    return make_poem

make_poem = poem()
print()
print(make_poem)



    





